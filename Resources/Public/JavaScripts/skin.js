(function ($) { // noConflict compatible
	$(function () { // domReady
		$(document).foundation();
		$('img').lazyInterchange({ threshold: 200 });

		// Smoothscroll
		var smoothScrollTo = function (target, offset, complete) {
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - offset
				}, 1000, null, complete);
				return false;
			}
		};

		$("a[href*='#']").click(function () {
			//Close MobileMenu when a Anchor link is clicked
			if ($(this).parents().find('.mainmenu').length) {
				document.getElementById('burger-toggle').checked = false;
			}
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				if (target.length) {
					smoothScrollTo(target, 60, null);
					return false;
				}
			}
		});

		/* Automatic buttons */
		$('.js a.js-button').click(function (e) {
			if ($(this).data('removeclass')) {
				$('body').removeClass($(this).data('removeclass'));
			}
			if ($(this).data('addclass')) {
				$('body').addClass($(this).data('addclass'));
			}
			if ($(this).data('toggleclass')) {
				$('body').toggleClass($(this).data('toggleclass'));
			}
			if ($(this).data('scrollto')) {
				smoothScrollTo($($(this).data('scrollto')), 0, null);
			}
			e.preventDefault();
		});

		$('.js label.js-button').click(function (e) {
			if ($(this).data('removeclass')) {
				$('body').removeClass($(this).data('removeclass'));
			}
			if ($(this).data('addclass')) {
				$('body').addClass($(this).data('addclass'));
			}
			if ($(this).data('toggleclass')) {
				$('body').toggleClass($(this).data('toggleclass'));
			}
		});

	}); // end domReady
})(jQuery); // end noConflict
