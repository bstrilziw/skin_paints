# INCLUDE CSS-Styled-Content base
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:fluid_styled_content/Configuration/TypoScript/Static/setup.txt">

# INCLUDE gridelements base
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:gridelements/Configuration/TypoScript/setup.ts">

# INCLUDE gridelements-layouts base
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pa_gridelements_layouts/Resources/Private/TypoScript/setup.ts">

# INCLUDE Extension custom settings
<INCLUDE_TYPOSCRIPT: source="FILE: ../Extensions/setup_extensions.ts">

# INCLUDE Main Setup
<INCLUDE_TYPOSCRIPT: source="FILE: ./Frontend/setup_page.ts">