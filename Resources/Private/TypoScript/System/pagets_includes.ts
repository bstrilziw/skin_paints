# Includes the TSconfig for gridelements layouts
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:pa_gridelements_layouts/Resources/Private/TypoScript/pagets.ts">

# Includes the TSconfig for pa_extended_fsc
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:pa_extended_fsc/Resources/Private/TypoScript/Backend/pagets_ttContent.ts">

# Includes the TSconfig for TCE default settings
<INCLUDE_TYPOSCRIPT: source="FILE:./Backend/pagets_tt_content.ts">

# INCLUDE Extension custom settings
<INCLUDE_TYPOSCRIPT: source="FILE:../Extensions/pagets_extensions.ts">