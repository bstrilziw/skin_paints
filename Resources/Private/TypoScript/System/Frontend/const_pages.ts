# General setup
config {
    # How to handle locallization
    sys_language_mode = strict

    # Records that are not locallized till be hidden
    sys_language_overlay = hideNonTranslated

    locale_all = de_DE.utf8

    # Language
    language = de
}

page.config {
    # Add L and print values to all links in TYPO3.
    linkVars = L(0-1)

    # Standard System Language ID
    sys_language_uid = 0

    # Language
    language = de

    favicon = EXT:skin_paints/Resources/Public/Icons/favicon.ico

    protocol = http
}

[globalString = _SERVER|HTTPS=on]
    page.config.protocol = https
[global]

page.settings {
    # PID Home
    home = 1

    logo {
        src = EXT:skin_paints/Resources/Public/Images/logo.svg
        width = 98
        height = 110
    }

    menu {
        # Select type og menu. Allowed "standardMenu", "anchorMenu"
        menuType = standardMenu

        # PID Main-Menu
        main = 1

        # PID Top-Menu
        top = 6

        # PID Footer-Menu
        footer = 9

        # Exclude pages in Main Menu
        mainnavExcludePages =
    }

    pages {
        # PID Footer Content
        footerContent = 30

        # PID Header Content
        headerContent = 23
    }

    cols {
        # colPos Main Content Column
        mainContentColumn = 0
    }

    keys {
        webfonts =
        googleFonts =
        googleTagManagerId =
    }

    footer {
        link = www.paints.de
        topContent = 0
        bottomContent = 1

        menuType = standard
    }
}

plugin.tx_skin_paints {
    view {
        templateRootPath = EXT:skin_paints/Resources/Private/Templates/
        partialRootPath = EXT:skin_paints/Resources/Private/Partials/
        layoutRootPath = EXT:skin_paints/Resources/Private/Layouts/
    }
}