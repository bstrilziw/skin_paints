### tt_content adjustments

# Remove unused section frames
TCEFORM.tt_content {
    section_frame {
        removeItems = 20,21,66
    }
    pa_extended_csc_header_style {
        addItems {
            h1-style = H1 Style
			h2-style = H2 Style
			h3-style = H3 Style
        }
    }
}