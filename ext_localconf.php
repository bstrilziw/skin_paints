<?php
if (!defined('TYPO3_MODE')) {
 	die ('Access denied.');
}

$_EXTCONF = unserialize($_EXTCONF);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:skin_paints/Resources/Private/TypoScript/System/pagets_includes.ts">');

// Add backend layouts
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['BackendLayoutFileProvider']['ext'][] = $_EXTKEY;

// RealURL autoconfiguration
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']['skin_paints'] = 'EXT:skin_paints/Classes/Hooks/RealUrl.php:Paints\\SkinPaints\\Hooks\\RealUrl->addConfig';


?>
